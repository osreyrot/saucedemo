from dotenv import load_dotenv
import json, os, random

def override_env(key, value):
    load_dotenv(dotenv_path=os.getcwd() + "/.env")
    if value:
        os.environ[key] = value

def get_env(env, default=''):
    load_dotenv(dotenv_path=os.getcwd() + "/.env")
    env = os.getenv(env)
    if not env:
        return default
    return env


def generate_random_number(start, end):
    ranNum = random.randint(int(start), int(end))
    return ranNum

