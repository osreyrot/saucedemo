*** Settings ***
Library    ../../Library/customLibrary.py
Resource    ../../Resource/Common.robot
Library    Collections

*** Variables ***
${product_btn_list}    (//div[@class='inventory_item_img'])

*** Keywords ***
Count Product In List
    ${countProBtn}    get element count    ${product_btn_list}
    [Return]    ${countProBtn}
Click Random Product To Card
    ${getProBtnCount}    Count Product In List
    ${ran}    generate random number    1    ${getProBtnCount}
    Run Keyword Until Success    scroll element into view    (//div[@class='inventory_item_img'])[${ran}]
    click element    (//div[@class='inventory_item_img'])[${ran}]


