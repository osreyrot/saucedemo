*** Settings ***
Library    ../../Library/customLibrary.py
Resource    ../../Resource/Common.robot



*** Variables ***
${username_env}    ${EMPTY}
${server_url_env}    ${EMPTY}
${password_env}    ${EMPTY}
${browser}    chrome
${username_txt}    //input[@id='user-name']
${password_txt}    //input[@id='password']
${login_btn}    //input[@id='login-button']
${menu_btn}    //button[@id='react-burger-menu-btn']
${close_menu_btn}    //button[@id='react-burger-cross-btn']

*** Keywords ***
Begin Web Test
    override env    SERVER_URL    ${server_url_env}
    override env    USER_NAME    ${username_env}
    override env    PASSWORD    ${password_env}
    open browser    about:blank    headlesschrome
    maximize browser window
End Web Test
    close all browsers
Go To Sauce Demo Site
    [Arguments]    ${url_env}
    Run Keyword Until Success    go to    ${url_env}
Enter User Name
    [Arguments]    ${user_name_env}
    Run Keyword Until Success    input text    ${username_txt}    ${user_name_env}
Enter Password
    [Arguments]    ${passowrd_env}
    Run Keyword Until Success    input text    ${password_txt}    ${passowrd_env}
Click Login Button
    Run Keyword Until Success    click element    ${login_btn}
Click Menu Button
    Run Keyword Until Success    click element    ${menu_btn}
Close Menu Button
    Run Keyword Until Success    click element    ${close_menu_btn}

# Full Steps

Login Succes
    ${server_url_env}    get env    SERVER_URL
    ${username_env}    get env    USER_NAME
    ${password_env}    get env    PASSWORD
    Go To Sauce Demo Site    ${server_url_env}
    Enter User Name    ${username_env}
    Enter Password    ${password_env}
    Click Login Button
    wait until page contains    Products
    Click Menu Button
    wait until page contains element    //nav[@class='bm-item-list']
    Close Menu Button


