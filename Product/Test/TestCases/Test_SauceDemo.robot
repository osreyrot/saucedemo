*** Settings ***
Library    SeleniumLibrary
Resource    ../../PageObject/Login.robot
Resource    ../../PageObject/AddItemToCart.robot
Resource    ../../PageObject/CheckoutItem.robot

Test Setup    Login.Begin Web Test
Test Teardown    Login.End Web Test

*** Test Cases ***
TC01 - Login Success
    ${server_url_env}    get env    SERVER_URL
    ${username_env}    get env    USER_NAME
    ${password_env}    get env    PASSWORD
    Go To Sauce Demo Site    ${server_url_env}
    Enter User Name    ${username_env}
    Enter Password    ${password_env}
    Click Login Button
    wait until page contains    Products
    Click Menu Button
    wait until page contains element    //nav[@class='bm-item-list']
    sleep    5s
TC02 - Add Item To Cart
    Login Succes
    Click Random Product To Card
    Add To Cart
    Click Cart Button
    wait until page contains element    //button[text() = 'Checkout']
    sleep    5s
TC03 - Checkout Item
    Login Succes
    Click Cart Button
    Count Item In List
    Click Checkout Button
    Enter First Name
    Enter Last Name
    Enter Postal Code
    wait until page contains     Checkout: Your Information
    sleep    2s
    Click Continue Button
    Click Finish Button
    sleep    2s
    wait until page contains    Thank you for your order!